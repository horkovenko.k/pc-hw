document.addEventListener('keydown', function(event) {
    let wasClick = event.code;
    if (wasClick.includes('Key')) {
        wasClick = wasClick.slice(-1);
    }
    let btnFind = document.getElementsByClassName("btn");
    for( let i = 0; i < btnFind.length; i++) {
        btnFind[i].style.backgroundColor = "black";
        if (wasClick === 'E') {
            btnFind[2].style.backgroundColor = "blue";
        } else if (btnFind[i].innerHTML.includes(wasClick)) {
            btnFind[i].style.backgroundColor = "blue";
        }
    }
});
