"use strict";

const photo = document.querySelectorAll('img');
const btn = document.querySelectorAll('button');

let currentValue = "0px";
const step = 500;

btn[1].onclick = function () {
    let leftValue = ( parseInt(currentValue) - step ) + 'px';

    if ( parseInt(currentValue) === -(photo.length * step) + step) {
        currentValue = '0px';
        leftValue = currentValue;
    } else {
        currentValue = leftValue;
    }

    for (let i = 0; i < photo.length; i++) {
        photo[i].style.left = leftValue;
    }
};

btn[0].onclick = function () {
    let rightValue = currentValue;

    if ( currentValue === "0px") {
        currentValue = -(photo.length * step) + step + 'px';
        rightValue = currentValue;
    } else {
        rightValue = (parseInt(currentValue) + step) + 'px';
        currentValue = rightValue;
    }

    for (let i = 0; i < photo.length; i++) {
        photo[i].style.left = rightValue;
    }
};

