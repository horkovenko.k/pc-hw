"use strict";

const contentElement = $(".tabs-content");
const titleElement = $(".tabs-title");

contentElement.hide();


for (let i = 0; i < titleElement.length; i++) {
    titleElement.eq(i).click(function () {
        titleElement.removeClass('active');
        titleElement.eq(i).addClass('active');

        contentElement.hide();
        contentElement.eq(i).show();
    });


}

