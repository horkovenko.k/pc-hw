"use strict";

let i = 0;
let timer = true;

const pic = document.getElementsByClassName("image-to-show");
const stopBtn = document.getElementById("stop-btn");
const startBtn = document.getElementById("start-btn");
const timerLb = document.getElementById("timer");

pic[i].style.display = "block";
next(9, 999);
function showPhoto() {
    for (let j = 0; j < pic.length; j++) {
        pic[j].style.display = "none";
    }
    i++;
    if (i === pic.length) {
        i = 0;
    }
    pic[i].style.display = "block";
}

let ms2 = "";
let s2 = "";

function next(s, ms) {
    // console.log('arguments', arguments)
        let timerId = setInterval(function () {
            ms2 = parseInt( timerLb.innerText.slice(-3) );
            timerLb.innerText = `Time left => ${s} : ${ms}`;
             if ( ms2 >= 100 ) {s2 = timerLb.innerText.slice(-7, -6);}
             if ( ms2 <= 99 && s2 >= 10 ) {s2 = timerLb.innerText.slice(-5, -4);}
             if ( ms2 <= 9 && s2 >= 0 ) {s2 = timerLb.innerText.slice(-5, -4);}
            console.log( ms2);
            if (ms < 0) {
                ms = 1000;
                s--;
                if (s <= -1) {
                    s = 9;
                    showPhoto();
                }
            }
            ms -= 4;
        }, 1);
        return timerId;
}




startBtn.onclick = function(){
    if (!timer) { // false, '', NaN, 0, null, undefined
        //console.log('args', timerLb.innerText.slice(-5, -4), timerLb.innerText.slice(-3));
        timer = next(s2, ms2);
    }
};

stopBtn.onclick = function(){
    //console.log(timer);
    //alert(s2);
    if (timer) {
        timer = clearInterval(timer); // timer = undefined
        //console.log('times in stop if', timer);

    }
};
