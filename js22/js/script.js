"use strict";

const table = document.createElement('table');
const page = document.body;

for (let it = 0; it < 30; it++) {
    let row = table.insertRow(it);
    for (let jt = 0; jt < 30; jt++) {
        let cell = row.insertCell(jt);
    }
}
document.body.append(table);

const td = document.querySelectorAll('td');

page.onclick = function(event) {
    let target = event.target;

    if (target == '[object HTMLTableCellElement]') {
        target.classList.toggle('is__click');
    } else if (target == '[object HTMLBodyElement]') {
        for (let i = 0; i < td.length; i++) {
            td[i].classList.toggle('is__click');
        }
    }
};
