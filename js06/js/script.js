let arr = ['hello', 'world', 23, '23', null, 523, undefined, null, true, false];

function filterBy( arrayName, typeName) {
    return arrayName.filter( item => typeof item !== typeName);
}

let a = filterBy(arr, 'number');

const result = arr.filter( item => typeof item !== 'number');
console.log(result);
console.log(a);

//// forEach применяет функцию для каждого элемента массива.