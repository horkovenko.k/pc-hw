"use strict";

const priceField = document.getElementById('price');
const currentPriceLb = document.getElementById('price__pointer');
const incorrectPriceLb = document.getElementById('incorrect__price');
const deleteCurrentPriceBtn = document.getElementById('delete__price');
const btnWrapper = document.getElementById('wrap');

deleteCurrentPriceBtn.onclick = function () {
    currentPriceLb.innerText = '';
    btnWrapper.style.display = 'none';
};

priceField.onblur = function () {
    if (priceField.value === "") {
        btnWrapper.style.display = 'none';
    } else if (isFinite(priceField.value) && parseFloat(priceField.value) >= 0) {
      btnWrapper.style.display = 'block';
      priceField.style.outlineColor = 'green';
      currentPriceLb.innerHTML = "Current price: " + priceField.value + " $";
  } else {
      incorrectPriceLb.style.opacity = '1';
      priceField.style.color = 'red';
      priceField.style.outlineColor = 'red';
      priceField.style.border = '2px solid red';
  }
};

priceField.onfocus = function () {
    incorrectPriceLb.style.opacity = '0';
    priceField.style.border = '1px solid #d1d1d1';
    priceField.style.color = 'black';
};