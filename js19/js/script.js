"use strict";

let developers = [5, 6, 5, 8, 9, 8];
let tasks = [150, 140, 180, 250, 390];
let dateOfDeadline = new Date(2020, 3, 16);

getDeadline(developers, tasks, dateOfDeadline);

function getDeadline(dev, task, dat) {
    let isCatch = false;
    let devPower = 0;
    let taskSummary = 0;
    let daysRequired = 0;
    let today = new Date();
    let daysAllowed = 0;

    for (let i = 0; i < dev.length; i++) {
        devPower += dev[i];
    }

    for (let i = 0; i < task.length; i++) {
        taskSummary += task[i];
    }

    daysRequired = Math.floor(taskSummary / devPower);

    daysAllowed = Math.floor((dat - today) / 8.64e+7);

    let sumHours = daysAllowed - daysRequired;

    if (sumHours >= 0) {
        alert(`All tasks will be complited in ${sumHours} days before deadline!`)
    } else {
        alert(`Developers need ${sumHours * -24} hours more to complete all tasks in backlog!`)
    }

    return isCatch;
}