jQuery(document).ready(function() {
$(function(){

    $('.anchor-section a').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top}, 2000);
    });

});


    const btn = $('.up-to-top');

    $(window).scroll(function() {
        if ($(window).scrollTop() < 500) {
            btn.addClass('show-nd');
        } else {
            btn.removeClass('show-nd');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, 2000);
    });

    $("#hide__button").click(function(){
        $("#hide-for-slide").slideToggle();
    });
});