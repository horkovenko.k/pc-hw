"use strict";

let user = {
    name : "John",
    age : 30,
    friends : ["Lisa", "Josh", "Mike"],
    birthday : {object : "Array"},
};

function deepClone(obj) {
    let clone;

    if (Array.isArray(obj)) {
        clone = [];
    } else {
        clone = {};
    }

    for (let key in obj) {
        if (typeof obj[key] === 'object') {
            clone[key] = deepClone(obj[key]);
        } else {
            clone[key] = obj[key];
        }
    }

    return clone;
}

let userClone = deepClone(user);

console.log(userClone);