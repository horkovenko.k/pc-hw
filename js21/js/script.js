"use strict";

const b = document.getElementById('btn');
const i = document.createElement('input');
const table = document.createElement('table');
const c = document.createElement('div');

let isInputVisible = false;
let isTableVisible = false;

i.id = 'input__numbers';
i.type = 'number';

b.onclick = function () {
    let val = i.value;
    if (val == '' && isInputVisible) {
        alert("Enter value!");
    } else if (val <= 0 && isInputVisible) {
        val = '';
        alert("Enter value more than 0!");
    } else if (val > 0 && isInputVisible) {
        c.style.width = `${val}px`;
        c.style.height = `${val}px`;
        c.style.borderRadius = '50%';

        if (!isTableVisible) {
            isTableVisible = true;
            for (let it = 0; it < 10; it++) {
                let row = table.insertRow(it);
                for (let jt = 0; jt < 10; jt++) {
                    let cell = row.insertCell(jt);
                    c.style.backgroundColor = getRandomColor();
                    cell.innerHTML = c.outerHTML;
                }
            }
            i.after(table);
            let tds = document.getElementsByTagName('td');
            for (let i = 0; i < tds.length; i++) {
                tds[i].onclick = function () {
                    this.remove();
                };
            }
        }

    } else if (!isInputVisible) {
        document.body.append(i);
        isInputVisible = true;
    }
};

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
