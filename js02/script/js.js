"use strict";

let num;
let check = true;

while (check) {
    num =  prompt("Number, please?", 0);
    if (isFinite(num) || Number.isInteger(num)) {
        check = false;
    }
}

for (let i = 0; i <= num; i += 5) {
    if ( num < 5 ) {
        console.log(`Sorry, no numbers`);
    } else if (i % 5 === 0 && i !== 0) {
        console.log(i);
    }
}

/*
let n1;
let n2;

let check = true;

while (check) {
    n1 = +prompt("Number 1st, please?", 0);
    n2 = +prompt("Number 2nd, please?", 0);

    Number.parseInt(n1);
    Number.parseInt(n2);

    if (  (n1 < n2) && isInteger(n1) && isInteger(n2) ) {
        check = false;
    }
}

for (let i = n1; i <= n2; i++) {
        if (isPrime(i)) {
            console.log(i);
        }
}

function isPrime(n) {
    if (n <= 1) return false;
    for (let i = 2; i <= n / 2; ++i) {
        if (n % i === 0) {
            return false;
        }
    }
    return true;
}


function isInteger(num) {
    return (num ^ 0) === num;
}
*/