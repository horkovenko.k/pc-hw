"use strict";

const size = document.getElementById('size__input');
const startButton = document.getElementById('start__btn');
const bomb = "<i class=\"fas fa-bomb\"></i>";
const flag = "<i class=\"fas fa-flag\"></i>";
let isTableVisible = false;
let gameOver = false;
let duplicate = [];
let userFind = 0;

startButton.hidden = true;

size.onfocus = function () {
    startButton.hidden = true;
};

size.onblur = function () {
    startGameBomber();
};

startButton.onclick = function () {
    startGameBomber();
};

function startGameBomber() {
    const table = document.createElement('table');
    gameOver = false;
    userFind = 0;
    duplicate = [];

    let s = +size.value;
    if (s <= 4 || s > 25) {
        alert("Enter value in range from 5 to 25!");
        size.value = '';
    } else {
        for (let i = 0; i < s; i++) {
            let row = table.insertRow(i);
            for (let j = 0; j < s; j++) {
                let col = row.insertCell(j);
            }
        }
    }
    startButton.after(table);

    let numberOfTables = document.querySelectorAll('table');
    if (numberOfTables.length === 2) {
        numberOfTables[1].remove();
    }

    setExplosive();
    fillNumbers();
    hideZeros();
    let currentValueHtml = flagArray();

    const cell = document.querySelectorAll('td');

    for (let i = 0; i < cell.length; i++) {
        cell[i].onclick = function () {
            if (this.innerHTML.includes('fa-bomb')) {
                gameOver = true;
                alert('Game over!');
                showBombs();
            }
            if (!gameOver) {
                this.style.opacity = '1';
                startButton.hidden = false;
            }

            let winNumber = countNumbers();

            if (this.innerHTML === '' && !gameOver) {
                showNeighbors(i);
            }

            if (this.innerHTML !== '' && !this.innerHTML.includes('fa-flag') && !gameOver) {

                if (!duplicate.includes(i)) {
                    duplicate.push(i);
                    userFind++;
                }
            }
            console.log(userFind)

            if (userFind === winNumber) {
                alert('You won!');
                gameOver = true;
                showBombs();
                showEmptyCells();
            }

        };


        cell[i].oncontextmenu = function () {
            if (this.innerHTML.includes('fa-flag') && !gameOver) {
                this.style.opacity = '0';
                this.innerHTML = currentValueHtml[i];
            } else if (!this.innerHTML.includes('fa-flag') && !gameOver) {
                this.innerHTML = flag;
                this.style.opacity = '1';
            }
        };

        cell[i].ondblclick = function () {
            let check = [1, 2, 3, 4, 5, 6, 7, 8, 9];

            if ( check.includes(+this.innerHTML) ) {
                showNumberNeighbors(i, +this.innerHTML);
            }
        };

    }
}

function showNumberNeighbors(i, currentNumber) {
    let s = +size.value;
    let td = document.querySelectorAll('td');
    let flagsAround = 0;

    let topBorder = getTopBorder(s);
    let bottomBorder = getBottomBorder(s);
    let leftBorder = getLeftBorder(s);
    let rightBorder = getRightBorder(s);


    if (!leftBorder.includes(i) && !td[i - 1].innerHTML.includes('fa-bomb')) {
        if (td[i - 1].innerHTML.includes('fa-flag')) {
            flagsAround++;
        }
    }


    if (!rightBorder.includes(i) && !td[i + 1].innerHTML.includes('fa-bomb')) {
        if (td[i + 1].innerHTML.includes('fa-flag')) {
            flagsAround++;
        }
    }


    if (!topBorder.includes(i) && !td[i - s].innerHTML.includes('fa-bomb')) {
        if (td[i - s].innerHTML.includes('fa-flag')) {
            flagsAround++;
        }
    }


    if (!bottomBorder.includes(i) && !td[i + s].innerHTML.includes('fa-bomb')) {
        if (td[i + s].innerHTML.includes('fa-flag')) {
            flagsAround++;
        }
    }


    if (!leftBorder.includes(i) && !bottomBorder.includes(i) && !td[i + s - 1].innerHTML.includes('fa-bomb')) {
        if (td[i + s - 1].innerHTML.includes('fa-flag')) {
            flagsAround++;
        }
    }


    if (!rightBorder.includes(i) && !bottomBorder.includes(i) && !td[i + s + 1].innerHTML.includes('fa-bomb')) {
        if (td[i + s + 1].innerHTML.includes('fa-flag')) {
            flagsAround++;
        }
    }


    if (!rightBorder.includes(i) && !topBorder.includes(i) && !td[i - s + 1].innerHTML.includes('fa-bomb')) {
        if (td[i - s + 1].innerHTML.includes('fa-flag')) {
            flagsAround++;
        }
    }


    if (!leftBorder.includes(i) && !topBorder.includes(i) && !td[i - s - 1].innerHTML.includes('fa-bomb')) {
        if (td[i - s - 1].innerHTML.includes('fa-flag')) {
            flagsAround++;
        }
    }

    if (flagsAround === currentNumber) {
        showNeighbors(i);
        td[i].style.backgroundColor = 'white';
    }
}

function flagArray() {
    let comp = [];

    let td = document.querySelectorAll('td');
    for (let i = 0; i < td.length; i++) {
        comp.push(td[i].innerHTML);
    }

    return comp;
}

function countNumbers() {
    let res = 0;
    let comp = [1, 2, 3, 4, 5, 6, 7, 8];

    let td = document.querySelectorAll('td');
    for (let i = 0; i < td.length; i++) {
        if (comp.includes(+td[i].innerHTML)) {
            res++;
        }
    }
    return res;
}

function showEmptyCells() {
    let td = document.querySelectorAll('td');
    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML === '') {
            td[i].style.opacity = '1';
        }
    }
}

function showBombs() {
    let td = document.querySelectorAll('td');
    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            td[i].style.opacity = '1';
        }
    }
}

function showNeighbors(i) {
    let s = +size.value;
    let td = document.querySelectorAll('td');
    td[i].style.backgroundColor = 'black';

    let topBorder = getTopBorder(s);
    let bottomBorder = getBottomBorder(s);
    let leftBorder = getLeftBorder(s);
    let rightBorder = getRightBorder(s);


    if (!leftBorder.includes(i) && !td[i - 1].innerHTML.includes('fa-bomb')) {
        td[i - 1].style.opacity = '1';
        if (td[i - 1].innerHTML === '') {
            td[i - 1].style.backgroundColor = 'black';
        } else if (!duplicate.includes(i - 1) && !td[i - 1].innerHTML.includes('fa-flag')) {
            duplicate.push(i - 1);
            userFind++;
        }
    }


    if (!rightBorder.includes(i) && !td[i + 1].innerHTML.includes('fa-bomb')) {
        td[i + 1].style.opacity = '1';
        if (td[i + 1].innerHTML === '') {
            td[i + 1].style.backgroundColor = 'black';
        } else if (!duplicate.includes(i + 1) && !td[i + 1].innerHTML.includes('fa-flag')) {
            duplicate.push(i + 1);
            userFind++;
        }
    }


    if (!topBorder.includes(i) && !td[i - s].innerHTML.includes('fa-bomb')) {
        td[i - s].style.opacity = '1';
        if (td[i - s].innerHTML === '') {
            td[i - s].style.backgroundColor = 'black';
        } else if (!duplicate.includes(i - s) && !td[i - s].innerHTML.includes('fa-flag')) {
            duplicate.push(i - s);
            userFind++;
        }
    }


    if (!bottomBorder.includes(i) && !td[i + s].innerHTML.includes('fa-bomb')) {
        td[i + s].style.opacity = '1';
        if (td[i + s].innerHTML === '') {
            td[i + s].style.backgroundColor = 'black';
        } else if (!duplicate.includes(i + s) && !td[i + s].innerHTML.includes('fa-flag')) {
            duplicate.push(i + s);
            userFind++;
        }
    }


    if (!leftBorder.includes(i) && !bottomBorder.includes(i) && !td[i + s - 1].innerHTML.includes('fa-bomb')) {
        td[i + s - 1].style.opacity = '1';
        if (td[i + s - 1].innerHTML === '') {
            td[i + s - 1].style.backgroundColor = 'black';
        } else if (!duplicate.includes(i + s - 1) && !td[i + s - 1].innerHTML.includes('fa-flag')) {
            duplicate.push(i + s - 1);
            userFind++;
        }
    }


    if (!rightBorder.includes(i) && !bottomBorder.includes(i) && !td[i + s + 1].innerHTML.includes('fa-bomb')) {
        td[i + s + 1].style.opacity = '1';
        if (td[i + s + 1].innerHTML === '') {
            td[i + s + 1].style.backgroundColor = 'black';
        } else if (!duplicate.includes(i + s + 1) && !td[i + s + 1].innerHTML.includes('fa-flag')) {
            duplicate.push(i + s + 1);
            userFind++;
        }
    }


    if (!rightBorder.includes(i) && !topBorder.includes(i) && !td[i - s + 1].innerHTML.includes('fa-bomb')) {
        td[i - s + 1].style.opacity = '1';
        if (td[i - s + 1].innerHTML === '') {
            td[i - s + 1].style.backgroundColor = 'black';
        } else if (!duplicate.includes(i - s + 1) && !td[i - s + 1].innerHTML.includes('fa-flag')) {
            duplicate.push(i - s + 1);
            userFind++;
        }
    }


    if (!leftBorder.includes(i) && !topBorder.includes(i) && !td[i - s - 1].innerHTML.includes('fa-bomb')) {
        td[i - s - 1].style.opacity = '1';
        if (td[i - s - 1].innerHTML === '') {
            td[i - s - 1].style.backgroundColor = 'black';
        } else if (!duplicate.includes(i - s - 1) && !td[i - s - 1].innerHTML.includes('fa-flag')) {
            duplicate.push(i - s - 1);
            userFind++;
        }
    }

}

function fillNumbers() {
    let s = +size.value;
    let td = document.querySelectorAll('td');
    let howManyBombs = 0;

    let topBorder = getTopBorder(s);
    let bottomBorder = getBottomBorder(s);
    let leftBorder = getLeftBorder(s);
    let rightBorder = getRightBorder(s);

    for (let i = 0; i < td.length; i++) {
        if (!td[i].innerHTML.includes('fa-bomb')) {
            td[i].innerHTML = '0';
        }
    }

    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            if (!leftBorder.includes(i) && !td[i - 1].innerHTML.includes('fa-bomb')) {
                td[i - 1].innerHTML = +td[i - 1].innerHTML + 1;
            }
        }
    }

    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            if (!rightBorder.includes(i) && !td[i + 1].innerHTML.includes('fa-bomb')) {
                td[i + 1].innerHTML = +td[i + 1].innerHTML + 1;
            }
        }
    }

    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            if (!topBorder.includes(i) && !td[i - s].innerHTML.includes('fa-bomb')) {
                td[i - s].innerHTML = +td[i - s].innerHTML + 1;
            }
        }
    }

    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            if (!bottomBorder.includes(i) && !td[i + s].innerHTML.includes('fa-bomb')) {
                td[i + s].innerHTML = +td[i + s].innerHTML + 1;
            }
        }
    }

    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            if (!leftBorder.includes(i) && !bottomBorder.includes(i) && !td[i + s - 1].innerHTML.includes('fa-bomb')) {
                td[i + s - 1].innerHTML = +td[i + s - 1].innerHTML + 1;
            }
        }
    }

    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            if (!rightBorder.includes(i) && !bottomBorder.includes(i) && !td[i + s + 1].innerHTML.includes('fa-bomb')) {
                td[i + s + 1].innerHTML = +td[i + s + 1].innerHTML + 1;
            }
        }
    }

    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            if (!rightBorder.includes(i) && !topBorder.includes(i) && !td[i - s + 1].innerHTML.includes('fa-bomb')) {
                td[i - s + 1].innerHTML = +td[i - s + 1].innerHTML + 1;
            }
        }
    }

    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML.includes('fa-bomb')) {
            if (!leftBorder.includes(i) && !topBorder.includes(i) && !td[i - s - 1].innerHTML.includes('fa-bomb')) {
                td[i - s - 1].innerHTML = +td[i - s - 1].innerHTML + 1;
            }
        }
    }


}

function isInTable(n) {
    let isIn = true;
    let s = +size.value;

    if (n < s || n > s ** 2) {
        return false;
    }

}

function setExplosive() {
    let numberOfBombs = Math.ceil(((+size.value) ** 2) / 6);
    let tdB = document.querySelectorAll('td');
    for (let i = 0; i < tdB.length; i++) {
        if (fixedProbabilityRandom() === 1 && numberOfBombs >= 1) {
            numberOfBombs--;
            tdB[i].innerHTML = bomb;
        }
    }
}

function fixedProbabilityRandom() {
    let maxNumber = +size.value - (+size.value / 2);

    let n = Math.floor(Math.random() * (maxNumber - 1)) + 1;
    return n;
}

function getTopBorder(s) {
    let res = [];
    for (let i = 0; i < s; i++) {
        res.push(i);
    }
    return res;
}

function getBottomBorder(s) {
    let res = [];
    for (let i = s ** 2 - s; i < s ** 2; i++) {
        res.push(i);
    }
    return res;
}

function getLeftBorder(s) {
    let res = [];
    for (let i = 0; i <= s ** 2 - s; i += s) {
        res.push(i);
    }
    return res;
}

function getRightBorder(s) {
    let res = [];
    for (let i = s - 1; i <= s ** 2; i += s) {
        res.push(i);
    }
    return res;
}

function hideZeros() {
    let td = document.querySelectorAll('td');
    for (let i = 0; i < td.length; i++) {
        if (td[i].innerHTML === '0') {
            td[i].innerHTML = '';
        }

        if (td[i].innerHTML.includes('fa-bomb') || isFinite(+td[i].innerHTML)) {
            td[i].style.opacity = '0';
        }
    }
}


