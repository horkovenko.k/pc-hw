"use strict";

let student = {
    name: "",
    lastName: "",
    table: [],
};

let sName = prompt("Enter name: ", "John");
let sLastName = prompt("Enter last name: ", "Wick");

student.name = sName;
student.lastName = sLastName;

while (true) {
    let subject = prompt("Enter subject name: ", "History");
    let mark = +prompt("Enter subject mark: ", "10");
    student.table.push(mark);
    let stop = confirm("Continue?");
    if (!stop) {
        break;
    }
}

let badMarksCounter = 0;
let average = 0;

for (let i = 0; i < student.table.length; i++) {
    if (student.table[i] < 4) {
        badMarksCounter++;
    }
    average += student.table[i];
}

if (badMarksCounter === 0) {
    alert("Student transferred to next course!");
}

if ((average / student.table.length) > 7 && badMarksCounter === 0) {
    alert("Student granted scholarship!");
}