document.addEventListener("DOMContentLoaded", function(event) {
    let array = ['1', '2', '3', 'sea', 'user', 23, [45, 55, [1, 23, null, "hello"]], 54, "bye!"];

    function createList(arr) {
        const newUl = document.createElement('ul');
        arr.map(elem => {
            const newLi = document.createElement('li');
            if (Array.isArray(elem)) {
                newUl.children[newUl.children.length - 1].append(createList(elem));
            } else {
                newUl.innerHTML += `<li>${elem}</li>`;
            }
            newLi.innerText = elem;
        });
        return newUl;
    }

    let res = document.getElementById('new');
    let time = document.getElementById('timer');

    function clear() {
        let current = 9;
        let timerId = setInterval(function () {
            time.innerText = `Time left: ${current}`;
            if (current == 0) {
                clearInterval(timerId);
                res.remove();
                time.remove();
            }
            current--;
        }, 1000);
    }

    res.append(createList(array));
    clear();
});