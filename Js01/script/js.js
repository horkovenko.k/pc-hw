let userAge;
let userName;

while (true) {
    userAge = +prompt('How old are you?', 0);
    userName = prompt('What is your name?', "Name");

    if ( isFinite(userAge) && !isFinite(+userName) ) break;
}

if (userAge < 18) {
    alert("You are not allowed to visit this website");
}
else if (userAge >= 18 && userAge <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert(`Welcome, ${userName}`);
    } else {
        alert("You are not allowed to visit this website");
    }
}
else if (userAge > 22) {
    alert(`Welcome, ${userName}`);
}
