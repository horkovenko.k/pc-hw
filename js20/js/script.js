let vehicles = [
    {
        name: 'Toyota',
        description: 'car',
        contentType: [
            {name: 'collection'},
            {year: 2002},
        ],
        locales: {
            name: 'Тойота',
            description: 'ru_RU'
        }
    },
    {
        name: 'Mercedes',
        description: 'van',
        contentType: [
            {name: 'collection'},
            {year: 2007},
        ],
        locales: {
            name: 'Мерседес',
            description: 'ru_RU'
        }
    },
    {
        name: 'BMW',
        description: 'sport',
        contentType: [
            {name: 'racing'},
            {year: 2018},
        ],
        specs: {
            features: [
                {color: 'red'},
                {drive: 'RWD'}
            ],
            performance: {
                hp: 374,
                topSpeed: 250
            }
        },
        locales: {
            nameLocale: 'БМВ',
            description: 'ru_RU'
        },
    }
];


function filterCollection(arr, str, flag, ...args) {
    let result = [];
    let find = str.split(' ');
    if (flag === false && args.length === 0) {
        for (let i = 0; i < arr.length; i++) {
            let t = JSON.stringify(arr[i]);
            for (let j = 0; j < find.length; j++) {
                if (t.includes(find[j]) && !result.includes(t)) {
                    result.push(t);
                }
            }
        }
    } else if (flag === true && args.length === 0) {
        let count = 0;
        for (let i = 0; i < arr.length; i++) {
            let t = JSON.stringify(arr[i]);
            for (let j = 0; j < find.length; j++) {
                if (t.includes(find[j])) {
                    count++;
                }
            }
            if (count === find.length && !result.includes(t)) {
                result.push(t);
            }
            count = 0;
        }
    } else if (flag === false && args.length > 0) {
        for (let i = 0; i < args.length; i++) {
            if (args[i].includes('.')) {
                args[i] = args[i].slice(args[i].indexOf('.') + 1);
            }
        }

        args = unique(args);
        let isHere = false;
        for (let i = 0; i < args.length; i++) {
            let t = JSON.stringify(arr[i]);
            if (t.includes(args[i])) {
                isHere = true;
            }
        }

        if (isHere) {
            if (flag === false) {
                for (let i = 0; i < arr.length; i++) {
                    let t = JSON.stringify(arr[i]);
                    for (let j = 0; j < find.length; j++) {
                        if (t.includes(find[j]) && !result.includes(t)) {
                            result.push(t);
                        }
                    }
                }
            }
            isHere = false;
        }

    } else if (flag === true && args.length > 0) {
        for (let i = 0; i < args.length; i++) {
            if (args[i].includes('.')) {
                args[i] = args[i].slice(args[i].indexOf('.') + 1);
            }
        }

        args = unique(args);
        let isHere = false;

        for (let i = 0; i < args.length; i++) {
            let t = JSON.stringify(arr[i]);
            if (t.includes(args[i])) {
                isHere = true;
            }
        }

        if (isHere) {
            if (flag === true) {
                let count = 0;
                for (let i = 0; i < arr.length; i++) {
                    let t = JSON.stringify(arr[i]);
                    for (let j = 0; j < find.length; j++) {
                        if (t.includes(find[j])) {
                            count++;
                        }
                    }
                    if (count === find.length && !result.includes(t)) {
                        result.push(t);
                    }
                    count = 0;
                }
            }

            isHere = false;
        }

    }

    if (result.length === 0) {
        result.push('No matches!');
    }
    return JSON.parse(result);
}

let res = filterCollection(vehicles, 'ru_RU Тойота', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description');
console.log( res );

///alert( JSON.stringify( vehicles[0] ) )

function unique(arr) {
    let result = [];

    for (let str of arr) {
        if (!result.includes(str)) {
            result.push(str);
        }
    }

    return result;
}