"use strict";

let number;

while (true) {
    number = prompt("Enter number: ", 0);
    if (isFinite(number)) break;
}

parseInt(number);

function f(n) {
    if (n === 1) return n;
    try {
        return n-- * f(n);
    } catch (e) {
        alert("You entered value less than 0, 0 or 1! Try again.")
    }

}

if (isFinite(f(number))) {
    alert(f(number));
}