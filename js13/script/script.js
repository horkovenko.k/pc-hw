"use strict";
const link = document.getElementById("main__link");
const btn = document.getElementById("change__theme__btn");

btn.onclick = function () {
    (link.getAttribute("href") === "css/style.css") ?
        link.setAttribute("href", "css/style2.css") :
        link.setAttribute("href", "css/style.css");

    localStorage.setItem('theme', link.getAttribute("href"));
};

if (localStorage.getItem('theme') === "css/style2.css") {
    link.setAttribute("href", "css/style2.css");
}