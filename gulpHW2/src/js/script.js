'use strict';

const btn = document.getElementById('button__320');
const dropdown = document.getElementById('hide__block');
const burger = document.querySelectorAll('.cross-line');

dropdown.style.display = 'none';
let isActive = false;

btn.onclick = function () {
    if (!isActive) {
        burger[0].classList.add('top-brgr-line');
        burger[1].classList.add('middle-brgr-line');
        burger[2].classList.add('bottom-brgr-line');
        dropdown.style.display = 'flex';
        isActive = true;
    } else {
        burger[0].classList.remove('top-brgr-line');
        burger[1].classList.remove('middle-brgr-line');
        burger[2].classList.remove('bottom-brgr-line');
        dropdown.style.display = 'none';
        isActive = false;
    }


};