"use strict";

let n1;
let n2;
let sign;
let check = true;

while (check) {
    n1 = parseInt( prompt("Enter number 1: ", 0) );
    n2 = parseInt( prompt("Enter number 2: ", 0) );
    sign = prompt("Enter math sign(+, -, *, /): ", 0);
    if (isFinite(n1) && isFinite(n2) && ( sign === `+` || sign === `-` || sign === `/` || sign === `*`  ) ) {
        check = false;
    }
}

function simpleCalc(n1, n2, s) {
    switch (s) {
        case `+`:
            console.log(n1 + n2);
            break;
        case `-`:
            console.log(n1 - n2);
            break;
        case `/`:
            console.log(n1 / n2);
            break;
        case `*`:
            console.log(n1 * n2);
            break;
        default:
            console.log("Input is incorrect!");
    }
}

simpleCalc(n1, n2, sign);