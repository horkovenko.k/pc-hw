"use strict";

let contentElement = document.getElementsByClassName("tabs-content");
let titleElement = document.getElementsByClassName("tabs-title");

for (let i = 0; i < titleElement.length; i++) {

    titleElement[i].onclick = function () {

        contentElement[i].style.display = 'block';
        titleElement[i].className += ' active';

        for (let j = 0; j < i; j++) {
            contentElement[j].style.display = 'none';
            titleElement[j].className = "tabs-title";
        }

        for (let c = i + 1; c < titleElement.length; c++) {
            contentElement[c].style.display = 'none';
            titleElement[c].className = "tabs-title";
        }
    };

    contentElement[i].style.display = 'none';
}

contentElement[0].style.display = 'block';
