"use strict";
//////// keyboard m = m+, n = m-, b = mrc, C = esc.
const screen = document.querySelector('input');
const btnList = document.querySelectorAll('.button');

let currentOperator = null;
let firstOperand = null;
let secondOperand = null;
let memory = null;

let isOperator = false;

btnList.forEach(function (btn) {
    btn.addEventListener('click', function (event) {
        let val = event.target;

        if (val.value === 'C') {
            screen.value = '';
            isOperator = false;
        }

        if ( val.classList.contains('black') && val.value !== 'C' ) {
            if (isOperator) {
                screen.value = '';
                isOperator = false;
            }
            screen.value += val.value;
        }

        if ( val.classList.contains('pink') ) {
            firstOperand = screen.value;
            currentOperator = val.value;
            isOperator = true;
        }

        if ( val.classList.contains('orange') ) {
            secondOperand = screen.value;
            if ( firstOperand !== null && secondOperand !== null && currentOperator !== null ) {
                screen.value = simpleCalculate(+firstOperand, +secondOperand, currentOperator);
            }
            isOperator = true;
        }

        if ( val.classList.contains('gray') ) {
            if ( val.value === 'm+' ) {
                memory += +screen.value;
            } else if ( val.value === 'm-' ) {
                memory -= +screen.value;
            } else if ( val.value === 'mrc' && +screen.value !== memory ) {
                screen.value = memory;
            } else if ( val.value === 'mrc' && +screen.value === memory ) {
                screen.value = '0';
                memory = 0;
            }
            isOperator = true;
         }

        screen.value = deleteZero(screen.value);
    });
});



function simpleCalculate( n1, n2, sign ) {
    switch (sign) {
        case '-':
            return n1 - n2;
        case '+':
            return n1 + n2;
        case '/':
            return n1 / n2;
        case '*':
            return n1 * n2;
    }
}

function deleteZero(str) {
    if ( str.length > 1 && str[0] === '0' && str[1] !== '.' ) {
        return str.slice(1);
    }

    if ( str.length === 1 && str[0] === '.' ) {
        return '0.';
    }

    let dotCounter = 0;
    for ( let i = 0; i < str.length; i++ ) {
        if (dotCounter < 1 && str.charAt(i) === '.') {
            dotCounter++;
        } else if (dotCounter >= 1 && str.charAt(i) === '.' ) {
            let str1 = str.slice( 0, i );
            let str2 = str.slice( i );
            str2 = str2.replace( '.', '');
            str = str1 + str2;
        }
    }

    return str;
}

document.addEventListener('keydown', function(event) {
    let val = event.code;

    if ( val === 'Numpad1' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '1';
    }
    if ( val === 'Numpad2' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '2';
    }
    if ( val === 'Numpad3' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '3';
    }
    if ( val === 'Numpad4' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '4';
    }
    if ( val === 'Numpad5' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '5';
    }
    if ( val === 'Numpad6' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '6';
    }
    if ( val === 'Numpad7' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '7';
    }
    if ( val === 'Numpad8' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '8';
    }
    if ( val === 'Numpad9' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '9';
    }
    if ( val === 'Numpad0' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '0';
    }
    if ( val === 'NumpadDecimal' ) {
        if (isOperator) {
            screen.value = '';
            isOperator = false;
        }
        screen.value += '.';
    }
    if ( val === 'Escape' ) {
        isOperator = false;
        screen.value = '';
    }

    if ( val === 'NumpadDivide' ) {
        firstOperand = screen.value;
        currentOperator = '/';
        isOperator = true;
    }

    if ( val === 'NumpadMultiply' ) {
        firstOperand = screen.value;
        currentOperator = '*';
        isOperator = true;
    }

    if ( val === 'NumpadSubtract' ) {
        firstOperand = screen.value;
        currentOperator = '-';
        isOperator = true;
    }

    if ( val === 'NumpadAdd' ) {
        firstOperand = screen.value;
        currentOperator = '+';
        isOperator = true;
    }

    if ( val === 'NumpadEnter' ) {
        secondOperand = screen.value;
        if ( firstOperand !== null && secondOperand !== null && currentOperator !== null ) {
            screen.value = simpleCalculate(+firstOperand, +secondOperand, currentOperator);
            isOperator = true;
        }
    }

    if ( val === 'KeyM' ) {
        isOperator = true;
        memory += +screen.value;
    }

    if ( val === 'KeyN' ) {
        isOperator = true;
        memory -= +screen.value;
    }

    if ( val === 'KeyB' ) {
        isOperator = true;
        if ( +screen.value !== memory ) {
            screen.value = memory;
        } else if (+screen.value === memory ) {
            screen.value = '0';
            memory = 0;
        }
    }

    screen.value = deleteZero(screen.value);

});
