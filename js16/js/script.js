"use strict";

let n = +prompt("Enter number n: ", 10);//0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144

function fibbonaci(n) {
    if (n === 0 || n === 1) return 0;
    if (n === 2 || n === 3) return 1;

    if (n === 0 || n === -1) return 0;
    if (n === -2 || n === -3) return -1;

    let check = true;

    if (n < 0) {
        check = false;
        n -= n*2;
    }

    let sum = 0;
    let n1 = 0;
    let n2 = 1;
    let n3 = 1;

    for (let i = 4; i <= n; i++) {
        sum = n2 + n3;
        n2 = n3;
        n3 = sum;
    }

    if (check) {
        sum = sum;
    } else {
        sum -= 2 * sum;
    }
    return sum;
}

console.log(fibbonaci(n));