function changeView(id) {
    if (id == "sprite1") {
        if (document.getElementById("pass-1st").getAttribute("type") == "password") {
            return document.getElementById("pass-1st").setAttribute("type", "text"),
                document.getElementById("sprite1").style.backgroundImage = "url('img/icons/opened-eye.png')";
        } else if (document.getElementById("pass-1st").getAttribute("type") == "text") {
            return document.getElementById("pass-1st").setAttribute("type", "password"),
                document.getElementById("sprite1").style.backgroundImage = "url('img/icons/closed-eye.png')";
        }
    } else if (id == "sprite2") {
        if ( document.getElementById("pass-2st").getAttribute("type") == "password" ) {
            return document.getElementById("pass-2st").setAttribute("type", "text"),
                document.getElementById("sprite2").style.backgroundImage = "url('img/icons/opened-eye.png')";
        } else if ( document.getElementById("pass-2st").getAttribute("type") == "text" ) {
            return document.getElementById("pass-2st").setAttribute("type", "password"),
                document.getElementById("sprite2").style.backgroundImage = "url('img/icons/closed-eye.png')";
        }
    }
}

function checkPass() {
    let pass1 = document.getElementById("pass-1st").value;
    let pass2 = document.getElementById("pass-2st").value;

    if (pass1 == pass2) {
        return document.getElementById("virus").style.opacity = "0",
               alert("You are welcome!");
    } else {
        return document.getElementById("virus").style.opacity = "1";
    }
}
