
function createNewUser() {
    let firstName = prompt("Enter first name: ", "King");
    let lastName = prompt("Enter last name: ", "Kong");
    let newUser = {
        firstName: firstName,
        lastName: lastName,

        get fullName() {
            return `${this.firstName} ${this.lastName}`;
        },

        set fullName(value) {
            [this.firstName, this.lastName] = value.split(" ");
        }
    };

    newUser.getLogin = function() {
        return  this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    };



    return newUser;
}

let user = createNewUser();

Object.defineProperty(user, "firstName", { value: "Вася", configurable: true, writable: false, enumerable: true });
Object.defineProperty(user, "lastName", { value: "Иванов", configurable: true, writable: false, enumerable: true });
/*
user.fullName = "Alice Cooper";
// Блок работает при writable свойстве true.
alert(user.firstName); // Alice
alert(user.lastName); // Cooper
*/
console.log( user.getLogin() );
